<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [ 'nombre', 'precio_venta', 'precio_costo','existencias'];
}