<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaP extends Model
{
    protected $fillable = [ 'no_factura', 'id_producto', 'cantidad_PV'];
}
