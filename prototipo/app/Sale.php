<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [ 'nombre_cliente', 'fecha_vencimiento', 'producto','cantidad','impuesto','total'];
}