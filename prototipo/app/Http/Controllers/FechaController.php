<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Sale;


class FechaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('fecha');
    }
    
    public function buscar(Request $request)
    {
        $fecha1=trim($request->get('desde'));
        $fecha2=trim($request->get('hasta'));
        $dates= Sale::whereBetween('created_at', [$fecha1, $fecha2])->get();;

        return view('fecha', compact('dates'));
    }
}