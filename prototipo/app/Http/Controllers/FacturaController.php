<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Sale;


class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('factura');
    }
    
    public static function factura($campo)
    {
        $ventas=Sale::orderBy('created_at','DESC')->first()->$campo;

        return $ventas;
    }
}
