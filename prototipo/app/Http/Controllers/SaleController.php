<?php

namespace App\Http\Controllers;

use App\Sale;
use App\Client;
use App\Product;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function imprimir()
    {

        $pdf= PDF::loadView('factura');
        return $pdf->stream('prueba.pdf');
    }
    public function cliente()
    {
        $clientes=Client::all();
        $productos=Product::all();

        return view('venta', compact('clientes','productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('venta');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pr= explode("|",$request->get('producto'));
        $nombre = $pr[1];
        $cant = $request->input('cantidad');
        $validatedData = $request->validate([
            'nombre_cliente' => 'required|max:50',
            'fecha_vencimiento' => 'required|max:50',
            'producto' => 'required|max:50',
            'cantidad' => 'required|numeric',
            'impuesto'=> 'required|numeric',
            'total'=> 'required|numeric',
        ]);
        Sale::create($validatedData);
        return redirect('/venta')->with('success', 'Venta guardada con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        //
    }
}
