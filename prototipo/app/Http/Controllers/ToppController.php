<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;
use App\Sale;

class ToppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('topp');
    }
    public function mostrar()
    {
        $mostrar = Sale::Select('producto','cantidad','total')
        
        ->orderBy('cantidad','DESC')  
        ->take(10)   
        ->get();

        return view('topp', compact('mostrar'));
    }

}