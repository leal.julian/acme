<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Sale;

class TopcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('topc');
    }
    public function cli()
    {
        $cliente = Sale::Select('nombre_cliente','cantidad','total')
        ->orderBy('cantidad','DESC') 
        ->get()
        ->unique('nombre_cliente');
        
        return view('topc', compact('cliente'));
    }

}