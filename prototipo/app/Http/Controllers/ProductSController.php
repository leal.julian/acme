<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function productS()
    {
        $shows = Product::all();
        return view('ProductS', compact('shows'));
    }
    public function create()
    {
        return view('create');
    }
    
    public function productV()
    {
        //$shows = Product::where('nombre', 'tornillo')->first();
        //echo $shows;
        return view('productV');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nombre' => 'required|max:50',
            'precio_venta' => 'required|numeric',
            'precio_costo' => 'required|numeric',
            'existencias' => 'required|numeric',
        ]);
        $show = Product::create($validatedData);
   
        return redirect('/productS')->with('success', 'Producto guardado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $show = Product::findOrFail($id);

        return view('edit', compact('show'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nombre' => 'required|max:50',
            'precio_venta' => 'required|numeric',
            'precio_costo' => 'required|numeric',
            'existencias' => 'required|numeric',
        ]);
        Product::whereId($id)->update($validatedData);
   
        return redirect('/productS')->with('update', 'Producto actualizado con éxito');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id', $id)->delete();
        
        return redirect('/productS')->with('delete', 'Producto eliminado con éxito');
    }
}
