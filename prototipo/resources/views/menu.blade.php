@extends('layouts.app')

@section('content')
<style>
    .content {
                text-align: center;
            }
    .margin{
    margin: 20px !important;
    }
    .margin-left{
    margin-left: 270px !important;
    }
    .flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header content">Herramienta contable</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <a class="btn btn-primary flex-center" href="http://127.0.0.1:8000/productS">Menú de productos</a> <br>
                <a class="btn btn-primary flex-center" href="http://127.0.0.1:8000/venta">Registrar venta</a> <br>
                <a class="btn btn-primary flex-center" href="http://127.0.0.1:8000/client">Registrar cliente</a> <br>

                @if (Auth::user()->rol == 'Jefe')
                    <a class="btn btn-primary flex-center" href="http://127.0.0.1:8000/empleados">Lista de empleados</a> <br>
                    <a class="btn btn-primary flex-center"  href="http://127.0.0.1:8000/topp">Top 10 productos más vendidos</a> <br>
                    <a class="btn btn-primary flex-center"  href="http://127.0.0.1:8000/fecha">Productos vendidos entre fechas</a> <br>
                    <a class="btn btn-primary flex-center"  href="http://127.0.0.1:8000/topc">Top 10 mejores clientes</a> <br>
                @endif
                                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
