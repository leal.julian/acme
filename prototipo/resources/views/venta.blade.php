@extends('layouts.app')
@php
    use \App\Http\Controllers\SaleController as imp;
@endphp
@section('content')

<style>
  .uper {
    margin-top: 40px;
  }
  .links > a {
    color: #636b6f;
    padding: 0 25px;
    font-size: 13px;
    font-weight: 600;
    letter-spacing: .1rem;
    text-decoration: none;
    text-transform: uppercase;
    }
    .top-right {
    position: absolute;
    right: 70px;
    top: 20px;
  }   
  .flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
<body>
    <div>
      @if (Route::has('login'))
          <div class="top-right links">
            @auth 
              <a href="{{ url('/menu') }}">Regresar</a>
            @endauth
          </div>
      @endif
    </div>    
</body>
<div class="card uper">
  <div class="card-header">
    Registro de ventas
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      
      <form name="v" method="post" action="{{route('sales.store')}}">
          @csrf
          <div class="form-group">
              <label for="nombre">Nombre de cliente:</label>
              <select class="form-control selectpicker" name="nombre_cliente" id="nombre_cliente" data-live-search="true" >
                @foreach($clientes as $cliente)
                  <option>{{$cliente->nombre}}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
              <label for="fecha">fecha_vencimiento:</label>
              <input  readonly class="form-control" name="fecha_vencimiento" value="<?php $fecha_actual = date("d-m-Y"); echo date("d-m-Y",strtotime($fecha_actual."+ 30 days"));?>"/>
          </div>
          <div class="form-group">
              <label>producto:</label>
              <select class="form-control selectpicker"  name="producto" id="producto" data-live-search="true">
                @foreach($productos as $producto)
                  <option value="{{$producto->precio_venta}}|{{$producto->nombre}}">{{$producto->nombre}}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
              <label for="cantidad">cantidad:</label>
              <input id="cantidad" type="text" class="form-control" name="cantidad"/>
          </div>
          <button id="agregar" type="button" class="btn btn-primary" value="add" onclick="obtenerDatos()">Agregar producto a factura</button>
          <div class="form-group flex-center">
          <h1>Factura de venta</h1>
          </div>    
          <table class="table table-striped" name="factura" id="factura">
          <thead>
          <tr>
            <td>Producto</td>
            <td>Cantidad</td>
            <td>Subtotal</td>
          </tr>
          </thead>
          </table>
          <div>
          <label >Impuesto:</label>
          <input  readonly id="imp" name="impuesto" value="0"><br>
          <label >Subtotal neto:</label>
          <input readonly id="subtotal" name="subtotal" value="0"><br>
          <label >Total:</label>
          <input readonly id="total" name="total" value="0">
          </div>
          <button type="submit" class="btn btn-primary" id="fact">Registrar venta</button>
      </form>
    <tbody>
  </div>
  <a class="btn btn-primary" href="{{route('factura')}}">Generar factura</a>
  
</div>

<script type="text/javascript">
  function obtenerDatos(){
    let valores = document.getElementById('producto').value.split("|")
    let producto = valores[1]
    let cantidad = parseInt(document.getElementById('cantidad').value)
    let subtotal = parseInt(document.getElementById('cantidad').value)*parseInt(valores[0])
    let fila = "<tr><td>"+producto+"</td><td>"+cantidad+"</td><td>"+subtotal+"</td></tr>"
    let btn = document.createElement("TR")
    btn.innerHTML = fila;
    document.getElementById("factura").appendChild(btn);
    let total = parseInt(document.getElementById("subtotal").value)+subtotal
    document.getElementById("subtotal").value = total;
    let impuesto = total*0.19
    document.getElementById("imp").value = impuesto;
    document.getElementById("total").value = impuesto+total;
  }

</script>

@endsection
