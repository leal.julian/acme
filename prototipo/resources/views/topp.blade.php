@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Top productos</title>
</head>
<style>
  .uper {
    margin-top: 40px;
  }
  
  .flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
  }

  .position-ref {
    position: relative;
  }

  .top-right {
    position: absolute;
    right: 50px;
    top: 20px;
  }
  .links > a {
    color: #636b6f;
    padding: 0 25px;
    font-size: 13px;
    font-weight: 600;
    letter-spacing: .1rem;
    text-decoration: none;
    text-transform: uppercase;
    }
</style>
<body>
<div>          
    <div class="links top-right">   
    <a href="{{ url('/menu') }}">Regresar</a>                            
    </div>           
  </div>   
</body>
<div>
    <div class="card uper">
        <div class="card-header">
            <h1 class= "flex-center">Top 10 de productos más vendidos</h1>   
        </div>
    </div>
    <div>
    <table class="table table-striped">
        <thead>
            <tr>
            <td>Nombre del producto</td>
            <td>total en dinero vendido</td>
            <td>Cantidad vendida</td>
        </tr>
        </thead>
    <tbody>
      @if(count($mostrar)<=0)
            <tr>
                <td colspan="7">No hay resultados</td>
            </tr>
        @else    
        @foreach($mostrar as $m)
        <tr>
            <td>{{$m->producto}}</td>
            <td>{{$m->total}}</td>
            <td>{{$m->cantidad}}</td>
            
        </tr>
        @endforeach
        @endif
        
    </tbody>
  </table>
    </div>
</div>
</html>
@endsection