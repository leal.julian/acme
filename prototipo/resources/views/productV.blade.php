@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
  
  .flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
  }

  .position-ref {
    position: relative;
  }

  .top-right {
    position: absolute;
    right: 70px;
    top: 20px;
  }
  .links > a {
    color: #636b6f;
    padding: 0 25px;
    font-size: 13px;
    font-weight: 600;
    letter-spacing: .1rem;
    text-decoration: none;
    text-transform: uppercase;
    }
</style>
<h1 class= "flex-center">Productos registrados</h1>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  @if(session()->get('delete'))
    <div class="alert alert-success">
      {{ session()->get('delete') }}  
    </div><br />
  @endif
  @if(session()->get('update'))
    <div class="alert alert-success">
      {{ session()->get('update') }}  
    </div><br />
  @endif

  <table class="table table-striped">
    <thead>
        <tr>
          <td>id</td> 
        </tr>
    </thead>
    <tbody>
        @foreach($shows as $show)
        <tr>
            <td>{{$show->id}}</td>
            <td>{{$show->nombre}}</td>
            <td>{{$show->precio_venta}}</td>
            <td>{{$show->precio_costo}}</td>
            <td>{{$show->existencias}}</td>                     
        </tr>
        @endforeach
    </tbody>
  </table>
</div>
@endsection

