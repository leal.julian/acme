@php
    use \App\Http\Controllers\FacturaController as fact;
@endphp
<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="./bs3.min.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Factura</title>
</head>
<style>
.top-right {
    position: absolute;
    left: 300px;
    top: 5px;
    font-size: 15px;
  } 
  .top-r {
    position: absolute;
    left: 500px;
    top: 5px;
    font-size: 15px;
  } 
  .letra {
    font-size: 17px;
    color: purple;
    border-collapse: separate;
    border-spacing: 40px 0px;
  }
  .letra1 {
    font-size: 15px;
    color: black;
  }
  .letra2 {
    font-size: 17px;
    color: purple;
    border-collapse: separate;
    border-spacing: 80px 0px;
  }
  

</style>
<div><img class="title m-b-md" src="https://i.ibb.co/zJFMGQ0/acme.png" width="200">
<h1 class="top-right">ACME Corporation<br>Nit 7956256625</h1>
<h2 class="top-r">Carrera 5 #9-41<br>Tuluá-Valle del cauca<br>Colombia</h2><br><br><br><br>
</div>

<div>
<table class="table table-striped letra">
    <thead>
        <tr>
          <td>Facturar a<br><br><font color="black">{{fact::factura('nombre_cliente')}}</font></td>
          <td>Fecha de facturación<br><br><font color="black">{{fact::factura('created_at')}}</font></td>
          <td>Fecha de vencimiento<br><br><font color="black">{{fact::factura('fecha_vencimiento')}}</font></td>
          <td>Número de factura<br><br><font color="black">{{fact::factura('no_factura')}}</font></td>
          <td>total<br><br><font color="black">{{fact::factura('total')}}</font></td>
        </tr>
    </thead>
</table>  
</div>
<hr width="500" >
<div class="row">
    <div class="col-xs-12">
        <table class="table table-condensed table-bordered table-striped letra2">
            <thead>
            <tr>
                <th>Precio unitario/Producto</th>
                <th>Cantidad</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
                <tr class="letra1">
                    <td><br><br>{{fact::factura('producto')}}</td>
                    <td><br><br>{{fact::factura('cantidad')}}</td>
                    <td><br><br>{{fact::factura('total')-fact::factura('impuesto')}}</td>
                </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2" class="text-right"><br><br><br>Subtotal</td>
                <td class="letra1"><br><br><br><br><br>{{fact::factura('total')-fact::factura('impuesto')}}</td>
            </tr>
            <tr>
                <td colspan="2" class="text-right">Impuestos</td>
                <td class="letra1">{{fact::factura('impuesto')}}</td>
            </tr>
            <tr>
                <td colspan="2" class="text-right">
                    <h4>Total</h4></td>
                    <td class="letra1">{{fact::factura('total')}}</td>
                <td>
                   
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
