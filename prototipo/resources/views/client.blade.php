@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
  .links > a {
    color: #636b6f;
    padding: 0 25px;
    font-size: 13px;
    font-weight: 600;
    letter-spacing: .1rem;
    text-decoration: none;
    text-transform: uppercase;
    }
    .top-right {
    position: absolute;
    right: 70px;
    top: 20px;
  }   
</style>
<body>
        <div>
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/menu') }}">Regresar</a>
                    @endauth
                </div>
            @endif
        </div>    
</body>
@if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
<div class="card uper">
  <div class="card-header">
    Registro de clientes
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('clients.store') }}">          
        @csrf            
          <div class="form-group">
              <label for="price">Nombre:</label>
              <input type="text" class="form-control" name="nombre"/>
          </div>
          <div class="form-group">
              <label for="price">Apellido:</label>
              <input type="text" class="form-control" name="apellido"/>
          </div>
          <div class="form-group">
              <label for="quantity">Dirección:</label>
              <input type="text" class="form-control" name="direccion"/>
          </div>
          <div class="form-group">
              <label for="quantity">Ciudad/departamento:</label>
              <input type="text" class="form-control" name="ciudad/departamento"/>
          </div>
          <button type="submit" class="btn btn-primary">Registrar cliente</button>
      </form>
  </div>
</div>
@endsection