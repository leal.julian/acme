@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
  .links > a {
    color: #636b6f;
    padding: 0 25px;
    font-size: 13px;
    font-weight: 600;
    letter-spacing: .1rem;
    text-decoration: none;
    text-transform: uppercase;
    }
    .top-right {
    position: absolute;
    right: 70px;
    top: 20px;
  }   
</style>
<body>
        <div>
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/productS') }}">Regresar</a>
                    @endauth
                </div>
            @endif
        </div>    
</body>
<div class="card uper">
  <div class="card-header">
    Registro de productos
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('products.store') }}">
              @csrf
          <div class="form-group">
              <label for="price">Nombre:</label>
              <input type="text" class="form-control" name="nombre"/>
          </div>
          <div class="form-group">
              <label for="price">precio venta:</label>
              <input type="text" class="form-control" name="precio_venta"/>
          </div>
          <div class="form-group">
              <label for="quantity">precio costo:</label>
              <input type="text" class="form-control" name="precio_costo"/>
          </div>
          <div class="form-group">
              <label for="quantity">existencias:</label>
              <input type="text" class="form-control" name="existencias"/>
          </div>
          <button type="submit" class="btn btn-primary">Registrar producto</button>
      </form>
  </div>
</div>
@endsection