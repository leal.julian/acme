@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
  
  .flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
  }

  .position-ref {
    position: relative;
  }

  .top-right {
    position: absolute;
    right: 50px;
    top: 20px;
  }
  .links > a {
    color: #636b6f;
    padding: 0 25px;
    font-size: 13px;
    font-weight: 600;
    letter-spacing: .1rem;
    text-decoration: none;
    text-transform: uppercase;
    }
</style>
@if(session()->get('delete'))
    <div class="alert alert-success" role="alert">
      {{session('delete')}}
    </div><br />
@endif  
<h1 class= "flex-center">Empleados</h1>
<body>
  <div>          
    <div class="links top-right">   
    <a href="{{ url('/menu') }}">Regresar</a>   <br>  <br>  <br>  <br>              
      <a href="http://127.0.0.1:8000/register">Registrar empleado</a>                   
    </div>           
  </div>    
</body>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  
  <table class="table table-striped">
    <thead>
        <tr>
          <td>usuario</td>
          <td>rol</td>
          <td colspan="1">eliminar empleado</td>
        </tr>
    </thead>
    <tbody>
        @foreach($shows as $show)
        <tr>
            <td>{{$show->usuario}}</td>
            <td>{{$show->rol}}</td>
            <td>
                <form action="{{ route('empleados.destroy', $show->usuario)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
</div>
@endsection