@extends('layouts.app')

@section('content')
<style>
    .flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
  }
  .uper {
    margin-top: 40px;
  }
  .top-r {
    position: absolute;
    left: 40px;
    top: 240px;
    font-size: 20px;
  } 
  .top-right {
    position: absolute;
    right: 50px;
    top: 20px;
  }
  .links > a {
    color: #636b6f;
    padding: 0 25px;
    font-size: 13px;
    font-weight: 600;
    letter-spacing: .1rem;
    text-decoration: none;
    text-transform: uppercase;
    }
</style>
<body>
    <div>          
        <div class="links top-right">   
            <a href="{{ url('/menu') }}">Regresar</a>                            
        </div>           
    </div>
</body>
<div class="card uper">
    <div class="card-header">
        <h1 class= "flex-center">Busqueda de productos por fechas</h1>   
    </div>
</div>
<div class="top-r">
    <form method="get" action="{{route('buscar')}}" >
    <label for="desde">Buscar productos desde:</label>
    <input type="date" name="desde" id="desde">
    <label for="hasta">Hasta:</label>
    <input type="date" name="hasta" id="hasta">
    <button type="submit" class="btn btn-primary">Buscar</button>
    </form>
    <div>
    <table class="table table-striped">
        <thead>
            <tr>
            <td>Nombre del cliente</td>
            <td>Precio/Producto</td>
            <td>Cantidad</td>
            <td>Fecha de vencimiento</td>
            <td>Impuesto</td>
            <td>Total</td>
            <td>Fecha de venta</td>
        </tr>
        </thead>
    <tbody>
        @if(count($dates)<=0)
            <tr>
                <td colspan="7">No hay resultados</td>
            </tr>
        @else    
        @foreach($dates as $date)
        <tr>
            <td>{{$date->nombre_cliente}}</td>
            <td>{{$date->producto}}</td>
            <td>{{$date->cantidad}}</td>
            <td>{{$date->fecha_vencimiento}}</td>
            <td>{{$date->impuesto}}</td>
            <td>{{$date->total}}</td>
            <td>{{$date->created_at}}</td>
        </tr>
        @endforeach
        @endif
    </tbody>
  </table>
    </div>
</div>

@endsection