<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('products', 'productSController');
Route::resource('sales', 'SaleController');
Route::resource('clients', 'ClientController');
Route::resource('empleados', 'EmpleadoController'); 
Auth::routes();


Route::get('/productV', 'ProductSController@productV')->name('productV');
Route::get('/topp', 'ToppController@index')->name('topp');
Route::get('/topc', 'TopcController@index')->name('topc');
Route::get('/topc', 'TopcController@cli')->name('mosc');
Route::get('/fecha', 'FechaController@index')->name('fecha');
Route::get('/fecha', 'FechaController@buscar')->name('buscar'); 
Route::get('/empleado', 'EmpleadoController@index')->name('empleado');
Route::get('/prueba', 'FacturaController@index')->name('prueba');
Route::get('/factura', 'SaleController@imprimir')->name('factura');
Route::get('/venta', 'SaleController@precio')->name('precio');
Route::get('/client', 'ClientController@create')->name('client');
Route::get('/menu', 'MenuController@index')->name('menu');
Route::get('/create', 'ProductSController@create')->name('create'); 
Route::get('/venta', 'SaleController@create')->name('venta');
Route::get('/venta', 'SaleController@cliente')->name('cliente');
Route::get('/topp', 'ToppController@mostrar')->name('mostrar');  
Route::get('/edit','ProductSController@edit')->name('edit');
Route::get('/productS', 'ProductSController@productS')->name('productS');
Route::get('/home', 'HomeController@index')->name('home');
