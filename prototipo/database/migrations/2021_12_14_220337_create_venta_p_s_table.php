<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentaPSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venta_p_s', function (Blueprint $table) {
            $table->integer('factura');
            $table->integer('id_producto');
            $table->integer('Cantidad_PV');
            $table->primary(['factura','id_producto']);
            $table->foreign('factura')->references('no_factura')->on('sales');
            $table->foreign('id_producto')->references('id')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venta_p_s');
    }
}
